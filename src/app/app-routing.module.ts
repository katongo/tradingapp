import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Shell } from './shell/shell.service';

const routes: Routes = [
// tslint:disable-next-line: max-line-length
  Shell.childRoutes([{ path: 'about', loadChildren: './about/about.module#AboutModule' }, { path: 'login' , loadChildren : './login/login.module#LoginModule'}, { path: 'signup' , loadChildren : './signup/signup.module#SignupModule'}, { path: 'dashboard' , loadChildren : './dashboard/dashboard.module#DashboardModule'}]),
  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
