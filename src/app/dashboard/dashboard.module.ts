import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '../core';
import { SharedModule } from '../shared';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ChartComponent } from '../chart/chart.component';
import { ChartModule } from 'primeng/chart';
import {DialogModule} from 'primeng/dialog';
import { CardModule } from 'primeng/card';

@NgModule({
  imports: [CommonModule, TranslateModule, CoreModule, SharedModule, DashboardRoutingModule, ChartModule, CardModule, DialogModule],
  declarations: [DashboardComponent, ChartComponent],
})
export class DashboardModule {}
