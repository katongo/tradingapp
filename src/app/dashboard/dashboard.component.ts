import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  display = false;
  show = false;

  data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
        {
            label: 'Bitcoin Price',
            data: [65, 59, 80, 81, 56, 55, 40],
            fill: false,
            borderColor: '#4bc0c0'
        },
        {
          label: 'Etherum Price',
          data: [23, 31, 52, 67, 60, 45, 39],
          fill: false,
          borderColor: '#3eca0b'
      },
      {
        label: 'DodgeCoin Price',
        data: [5, 9, 2, 1, 6, 7, 13],
        fill: false,
        borderColor: '#d4a9ff'
    }
    ]
};
  constructor() { }
  showDialog() {
    this.display = !this.display;
}
showDialogg() {
  this.show = !this.show;
}

  ngOnInit() {
  }

}



