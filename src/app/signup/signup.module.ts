import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '../core';
import { SharedModule } from '../shared';
import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';

@NgModule({
  imports: [CommonModule, TranslateModule, CoreModule, SharedModule, SignupRoutingModule],
  declarations: [SignupComponent],
})
export class SignupModule {}
