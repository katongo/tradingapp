import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
        {
            label: 'Bitcoin Price',
            data: [65, 59, 80, 81, 56, 55, 40],
            fill: false,
            borderColor: '#4bc0c0'
        },
        {
          label: 'Etherum Price',
          data: [2.3, 5.9, 8.0, 8.1, 5.6, 5.5, 4.0],
          fill: false,
          borderColor: '#3eca0b'
      },
      {
        label: 'DodgeCoin Price',
        data: [5, 9, 2, 1, 6, 7, 13],
        fill: false,
        borderColor: '#d4a9ff'
    }
    ]
};

  constructor(  ) { }

  ngOnInit() {
  }


}
